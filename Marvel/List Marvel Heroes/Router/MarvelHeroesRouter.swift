//
//  MarvelHeroesRouter.swift
//  Marvel
//
//  Created by Daniel Griso Filho on 5/19/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import Foundation

protocol MarvelHeroesRouterProtocol {
    func detailMarvelHeroesView(withHeroInfo: MarvelHeroesEntity)
}

class MarvelHeroesRouter: BaseRouter {
    
    //MARK: - Class Funtions
    static func listMarvelHeroesViewController() -> MarvelHeroesViewController {
        
        let viewController: MarvelHeroesViewController = MarvelHeroesViewController()
        
        let router: MarvelHeroesRouter = MarvelHeroesRouter(viewController: viewController)
        
        let interactor: MarvelHeroesInteractor = MarvelHeroesInteractor()
        
        let presenter: MarvelHeroesPresenter = MarvelHeroesPresenter(router: router, interactor: interactor, view: viewController)
        
        interactor.presenter = presenter
        viewController.presenter = presenter
        return viewController
    }
    
    func showDetailMarvelHeroes(withHeroInfo hero: MarvelHeroesEntity) {
        let detailMarvelHeroes = DetailMarvelHeroesRouter.detailMarvelHeroesViewCotroller()
        detailMarvelHeroes.heroName = hero.name
        detailMarvelHeroes.heroDescription = hero.description
        let imageURL = "\(hero.thumbnailPath!).\(hero.thumbnailExtension!)"
        detailMarvelHeroes.heroImage = imageURL
        self.pushToView(viewController: detailMarvelHeroes, animated: true)
    }
}

//MARK: - Class extension for MarvelHeroesRouter Protocols
extension MarvelHeroesRouter: MarvelHeroesRouterProtocol{
    func detailMarvelHeroesView(withHeroInfo hero: MarvelHeroesEntity) {
        self.showDetailMarvelHeroes(withHeroInfo: hero)
    }
}
