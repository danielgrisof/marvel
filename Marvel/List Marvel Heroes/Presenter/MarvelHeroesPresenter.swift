//
//  MarvelHeroesPresenter.swift
//  Marvel
//
//  Created by Daniel Griso Filho on 5/19/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import Foundation

protocol MarvelHeroesPresenterProtocol {
    func getMarvelInformations()
    func resultMarvelHeroes(withHeroesArray: [MarvelHeroesEntity])
    func didSelect(heroAt: MarvelHeroesEntity)
}

class MarvelHeroesPresenter {
    
    //MARK: - Local variables
    private let router: MarvelHeroesRouterProtocol?
    private let interactor: MarvelHeroesInteractorProtocol?
    private let view: MarvelHeroesViewControllerProtocol?
    
    //MARK: - Variables initialization
    init(router: MarvelHeroesRouterProtocol, interactor: MarvelHeroesInteractorProtocol, view: MarvelHeroesViewControllerProtocol) {
        self.router = router
        self.interactor = interactor
        self.view = view
    }
    
    //MARK: - Class Functions
    private func getMarvelCharacters(){
        let characters = "characters"
        self.interactor?.getMarvelInfo(about: characters)
    }
}

//MARK: - Class extension for MarvelHeroesPresenter Protocols
extension MarvelHeroesPresenter: MarvelHeroesPresenterProtocol{
    func didSelect(heroAt: MarvelHeroesEntity) {
        self.router?.detailMarvelHeroesView(withHeroInfo: heroAt)
    }
    
    func resultMarvelHeroes(withHeroesArray heroesArray: [MarvelHeroesEntity]) {
        if heroesArray.count < 0 {
            self.view?.showAlert(title: "Nenhum Heroi", message: "Não foi encontrado nenhum heroi no momento, por favor tentar mais tarde.")
        }else{
            self.view?.heroesList(withHeroesArray: heroesArray)
        }
    }

    func getMarvelInformations() {
        self.getMarvelCharacters()
    }
    
}
