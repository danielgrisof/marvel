//
//  MarvelHeroesInteractor.swift
//  Marvel
//
//  Created by Daniel Griso Filho on 5/19/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

protocol MarvelHeroesInteractorProtocol {
    func getMarvelInfo(about: String)
}

class MarvelHeroesInteractor {
    
    //MARK: - Local Variables
    var presenter: MarvelHeroesPresenterProtocol!
    private let BASEURL = "https://gateway.marvel.com:443/v1/public/"
    private let PUBLICAPIKEY = "db12fa397c0f047a3615dac1d8afc959"
    private let TS = 100
    private let PRIVATEAPIKEY = "eb78d36ab9107a94740fda15012cba99d1bf2c43"
    private let hash = "48a85f3618b1a8cf00276a50c62802f2"
    
    //MARK: - Class Functions
    private func getInformations(topic: String){
        
        let parameters: Parameters = ["ts": TS, "apikey": PUBLICAPIKEY, "hash": hash]
        let finalURL = "\(BASEURL)\(topic)"

        Alamofire.request(finalURL, parameters: parameters).responseJSON { (response) in
            
            let result = response.result.value
            
            if let jsonResult = result as? [String: AnyObject] {
                if let data = jsonResult["data"] as? [String: AnyObject]{
                    if let results = data["results"] as? [[String: AnyObject]]{
                        let marvelHeroes = Mapper<MarvelHeroesEntity>().mapArray(JSONArray: results)
                        self.presenter.resultMarvelHeroes(withHeroesArray: marvelHeroes)
                    }
                }
            }
        }
    }
}

//MARK: - Class extension for MarvelHeroesInteractor Protocols
extension MarvelHeroesInteractor: MarvelHeroesInteractorProtocol{
    func getMarvelInfo(about: String) {
        self.getInformations(topic: about)
    }
}
