//
//  MarvelHeroesViewController.swift
//  Marvel
//
//  Created by Daniel Griso Filho on 5/19/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import UIKit

protocol MarvelHeroesViewControllerProtocol {
    func showAlert(title: String, message: String)
    func heroesList(withHeroesArray: [MarvelHeroesEntity])
}

class MarvelHeroesViewController: UIViewController {

    //MARK: - Local Variables
    var presenter: MarvelHeroesPresenterProtocol?
    lazy var marvelHeroesArray = [MarvelHeroesEntity]()
    
    //MARK: - Local Properties
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Life Cicle APP
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configurateNavigationBar()
        self.registerCustomCell()
        self.presenter?.getMarvelInformations()
    }
    
    //MARK: - Class Functions
    func configurateNavigationBar(){
        self.title = "Marvel Heroes"
        let backButton = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        backButton.tintColor = UIColor.black
        self.navigationItem.backBarButtonItem = backButton
    }
    
    func registerCustomCell(){
        tableView.register(UINib(nibName: "MarvelHeroesCustomCell", bundle: nil), forCellReuseIdentifier: "marvelHeroesCustomCell")
    }

}

//MARK: - Class extension for UITableView Protocols
extension MarvelHeroesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.marvelHeroesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "marvelHeroesCustomCell", for: indexPath) as? MarvelHeroesCustomCell {
            cell.configureCell(withHeroesArray: [marvelHeroesArray[indexPath.row]])
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(integerLiteral: 200)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter?.didSelect(heroAt: marvelHeroesArray[indexPath.row])
    }
    
}

//MARK: - Class extension for MarvelHeroesViewController Protocols
extension MarvelHeroesViewController: MarvelHeroesViewControllerProtocol{
    func heroesList(withHeroesArray: [MarvelHeroesEntity]) {
        self.marvelHeroesArray = withHeroesArray
        self.tableView.reloadData()
    }
    
    func showAlert(title: String, message: String) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let firstButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertView.addAction(firstButton)
        present(alertView, animated: true, completion: nil)
    }
    
}
