//
//  MarvelHeroesCustomCell.swift
//  Marvel
//
//  Created by Daniel Griso Filho on 5/19/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import UIKit
import SDWebImage

class MarvelHeroesCustomCell: UITableViewCell {

    //MARK: - Local Properties
    @IBOutlet weak var lblHeroName: UILabel!
    @IBOutlet weak var imgHero: UIImageView!

    //MARK: - Class Functions
    func configureCell(withHeroesArray heroArray: [MarvelHeroesEntity]){
        for hero in heroArray {
            self.lblHeroName.text = hero.name
            
            let urlImage = "\(hero.thumbnailPath!).\(hero.thumbnailExtension!)"
            
            self.imgHero.sd_setImage(with: URL(string: urlImage), placeholderImage: UIImage(named: "placeHolder"), options: .cacheMemoryOnly, completed: nil)
        }
    }
    
}
