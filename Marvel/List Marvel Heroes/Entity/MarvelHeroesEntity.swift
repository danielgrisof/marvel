//
//  MarvelHeroesEntity.swift
//  Marvel
//
//  Created by Daniel Griso Filho on 5/19/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import Foundation
import ObjectMapper

class MarvelHeroesEntity: Mappable {
    
    //MARK: - Local Variables
    var id: Int?
    var name: String?
    var description: String?
    var modified: String?
    var thumbnailPath: String?
    var thumbnailExtension: String?
    var resourceURI: String?
    var comicsAvalible: Double?
    var comicsCollectionURI: String?
    var comicsItems: [[String:String]]?
    var comicsReturned: Double?
    var serieAvalible: Double?
    var serieCollectionURI: String?
    var serieItems: [[String:String]]?
    var serieReturned: Double?
    var storiesAvalible: Double?
    var storiesCollectionURI: String?
    var storiesItems: [[String:String]]?
    var storiesReturned: Double?
    var eventsAvalible: Double?
    var eventsCollectionURI: String?
    var eventsItems: [[String:String]]?
    var eventsReturned: Double?
    var urls: [[String:String]]?
    
    //MARK: - Initialization
    required init?(map: Map) {
    }
    
    //MARK: - Class Functions
    func mapping(map: Map) {
        
        id <- map["id"]
        name  <- map["name"]
        description  <- map["description"]
        modified  <- map["modified"]
        thumbnailPath  <- map["thumbnail.path"]
        thumbnailExtension  <- map["thumbnail.extension"]
        resourceURI  <- map["resourceURI"]
        comicsAvalible  <- map["comics.available"]
        comicsCollectionURI  <- map["comics.collectionURI"]
        comicsItems  <- map["comics.items"]
        comicsReturned  <- map["comics.returned"]
        serieAvalible  <- map["series.available"]
        serieCollectionURI  <- map["series.collectionURI"]
        serieItems  <- map["series.items"]
        serieReturned  <- map["series.returned"]
        storiesAvalible  <- map["stories.available"]
        storiesCollectionURI  <- map["stories.collectionURI"]
        storiesItems  <- map["stories.items"]
        storiesReturned  <- map["stories.returned"]
        eventsAvalible  <- map["events.available"]
        eventsCollectionURI  <- map["events.collectionURI"]
        eventsItems  <- map["events.items"]
        eventsReturned  <- map["events.returned"]
        urls  <- map["urls"]
    }
}
