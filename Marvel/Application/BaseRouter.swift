//
//  BaseRouter.swift
//  Marvel
//
//  Created by Daniel Griso Filho on 5/19/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import UIKit

class BaseRouter {
    
    // MARK: - Variables
    weak var viewController: UIViewController?
    
    // MARK: - Initialization
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
    
    //MARK: - Functions
    func pushToView(viewController: UIViewController, animated: Bool){
        self.viewController?.navigationController?.pushViewController(viewController, animated: animated)
    }
    
    func presentToView(viewController: UIViewController, animated: Bool){
        self.viewController?.navigationController?.present(viewController, animated: animated, completion: nil)
    }
}
