//
//  DetailMarvelHeroesInteractor.swift
//  Marvel
//
//  Created by Daniel Griso Filho on 5/20/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import Foundation

protocol DetailMarvelHeroesInteractorProtocol {
    
}

class DetailMarvelHeroesInteractor {
    
    //MARK: - Local Variables
    var presenter: DetailMarvelHeroesPresenterProtocol?
    
}

//MARK: - Class extension for DetailMarvelHeroesInteractor Protocols
extension DetailMarvelHeroesInteractor: DetailMarvelHeroesInteractorProtocol {
    
}
