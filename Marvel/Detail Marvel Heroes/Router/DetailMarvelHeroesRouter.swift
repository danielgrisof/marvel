//
//  DetailMarvelHeroesRouter.swift
//  Marvel
//
//  Created by Daniel Griso Filho on 5/20/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import UIKit

protocol DetailMarvelHeroesRouterProtocol {
    
}

class DetailMarvelHeroesRouter: BaseRouter {
    
    //MARK: - Class Funtions
    static func detailMarvelHeroesViewCotroller() -> DetailMarvelHeroesViewController {
        
        let viewController: DetailMarvelHeroesViewController = DetailMarvelHeroesViewController()
        
        let router: DetailMarvelHeroesRouter = DetailMarvelHeroesRouter(viewController: viewController)
        
        let interactor: DetailMarvelHeroesInteractor = DetailMarvelHeroesInteractor()
        
        let presenter: DetailMarvelHeroesPresenter = DetailMarvelHeroesPresenter(router: router, interactor: interactor, view: viewController)
        
        interactor.presenter = presenter
        viewController.presenter = presenter
        
        return viewController
    }
}

//MARK: - Class extension for DetailMarvelHeroesRouter Protocols
extension DetailMarvelHeroesRouter: DetailMarvelHeroesRouterProtocol {
    
}
