//
//  DetailMarvelHeroesPresenter.swift
//  Marvel
//
//  Created by Daniel Griso Filho on 5/20/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import Foundation

protocol DetailMarvelHeroesPresenterProtocol {
    
}

class DetailMarvelHeroesPresenter {
    
    //MARK: - Local variables
    var router: DetailMarvelHeroesRouterProtocol?
    var interactor: DetailMarvelHeroesInteractorProtocol?
    var view: DetailMarvelHeroesViewControllerProtocol?
    
    //MARK: - Variables initialization
    init(router: DetailMarvelHeroesRouterProtocol, interactor: DetailMarvelHeroesInteractorProtocol, view: DetailMarvelHeroesViewControllerProtocol) {
        self.router = router
        self.interactor = interactor
        self.view = view
    }
    
}

//MARK: - Class extension for DetailMarvelHeroesPresenter Protocols
extension DetailMarvelHeroesPresenter: DetailMarvelHeroesPresenterProtocol {
    
}
