//
//  DetailMarvelHeroesViewController.swift
//  Marvel
//
//  Created by Daniel Griso Filho on 5/20/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import UIKit
import SDWebImage

protocol DetailMarvelHeroesViewControllerProtocol {
    
}

class DetailMarvelHeroesViewController: UIViewController {
    
    //MARK: - Local Properties
    @IBOutlet weak var imgHero: UIImageView!
    @IBOutlet weak var txtViewHeroDescription: UITextView!
    
    //MARK: - Local Variables
    var presenter: DetailMarvelHeroesPresenterProtocol?
    var heroName: String?
    var heroDescription: String?
    var heroImage: String?

    //MARK: - Life Cicle APP
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureFields()
    }
    
    //MARK: - Class Functions
    func configureFields(){
        self.title = heroName
        self.txtViewHeroDescription.text = heroDescription
        guard let heroImageSafe = heroImage else {return}
        self.imgHero.sd_setImage(with: URL(string: heroImageSafe), placeholderImage: UIImage(named: "placeHolder"), options: .cacheMemoryOnly, completed: nil)
    }

}

//MARK: - Class extension for DetailMarvelHeroesViewController Protocols
extension DetailMarvelHeroesViewController: DetailMarvelHeroesViewControllerProtocol {
    
}
